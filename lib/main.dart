import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget titleSection = Container(
      child: Row(children: [
        Stateful(),
      ]),
    );

    return MaterialApp(
        title: 'Stateful Widgets',
        theme: ThemeData(
          primarySwatch: Colors.teal,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: Scaffold(
          appBar: AppBar(
            title: Text('481 HW3: Stateful Widgets'),
          ),
          body: ListView(
            children: [
              titleSection,
            ],
          ),
        ));
  }
// This widget is the root of your application.

}

class Stateful extends StatefulWidget {
  @override
  _StatefulState createState() => _StatefulState();
}

class _StatefulState extends State<Stateful> {
  var _imageSelection;

  void _swapImage(var v) {
    setState(() {
      _imageSelection = v;
    });
  }

  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Image.asset('$_imageSelection', width: 350, height: 250),
          Row(
            children: [
              RaisedButton(
                onPressed: () => _swapImage('images/LivingRoom.png'),
                color: Colors.teal,
                padding: EdgeInsets.all(10.0),
                child: Column(
                  // Replace with a Row for horizontal icon + text
                  children: <Widget>[
                    Icon(Icons.weekend_rounded),
                    Text("Living Room")
                  ],
                ),
              ),
              RaisedButton(
                onPressed: () => _swapImage('images/Bedroom.png'),
                color: Colors.teal,
                padding: EdgeInsets.all(10.0),
                child: Column(
                  // Replace with a Row for horizontal icon + text
                  children: <Widget>[
                    Icon(Icons.airline_seat_individual_suite_rounded),
                    Text("Bedroom")
                  ],
                ),
              ),
              RaisedButton(
                onPressed: () => _swapImage('images/ShowAll.png'),
                color: Colors.teal,
                padding: EdgeInsets.all(10.0),
                child: Column(
                  // Replace with a Row for horizontal icon + text
                  children: <Widget>[
                    Icon(Icons.apps_rounded),
                    Text("Show All")
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
